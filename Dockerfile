FROM httpd:2.4

COPY ./htdocs /usr/local/apache2/htdocs
COPY ./ssl/localhost.crt /ssl/localhost.crt
COPY ./ssl/localhost.key /ssl/localhost.key
COPY ./httpd.conf /usr/local/apache2/conf/httpd.conf
COPY ./httpd-vhosts.conf /usr/local/apache2/conf/extra/httpd-vhosts.conf
